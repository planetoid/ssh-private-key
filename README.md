This is a companion project for <https://docs.gitlab.com/ee/ci/ssh_keys/>.

If you are looking for the Alpine version, check this comment https://gitlab.com/gitlab-examples/ssh-private-key/issues/4#note_35042568.

* docker 使用 SSH public key 存取 Git repository
* 下載 repository 檔案到 docker image 建立的 container 內，所以下 `pwd`指令看到的是 

```bash
$ pwd
/builds/username/ssh-private-key

$ ls
README.md

$ git clone git@gitlab.com:gitlab-examples/ssh-private-key.git
Cloning into 'ssh-private-key'...
Job succeeded
```

Ref

* GitLab and SSH keys | GitLab https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair
* 